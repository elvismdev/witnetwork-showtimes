<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/siteCheckAjax/{category}/{date}", name="siteCheckAjax")
     * @param string $category Category name provided by front-end
     * @param string $date (Y-m-d) Post/Show date provided by front-end
     * @return bool JsonResponse
     * @example http://site/siteCheckAjax/Featured/2015-02-28
     */
    public function siteCheck($category = '', $date = '')
    {
        if (!$date)
            $date = new \DateTime('now');
        else
            $date = new \DateTime($date);

        // Check for category name provided by calendar
        $termManager = $this->get('ekino.wordpress.manager.term');
        $term = $termManager->findOneBy(array('name' => $category));
        if (!$term)
            return new JsonResponse(array('status' => false, 'msj' => 'Term not found'));

        // Is a real category?
        $tax = $this->isCategory($term->getId());
        if (!$tax)
            return new JsonResponse(array('status' => false, 'msj' => 'Category not found'));

        // Find posts by date
        $postManager = $this->get('ekino.wordpress.manager.post');
        $posts = $postManager->findByDate($date);

        if (!$posts)
            return new JsonResponse(array('status' => false, 'msj' => 'Posts not found by given date'));

        foreach ($posts as $post) {
            // Status?
            if ($post->getStatus() != 'publish' && $post->getStatus() != 'future')
                continue;

            if ($this->isPostCategory($post->getId(), $tax))
                return new JsonResponse(array('status' => true, 'msj' => $post->getId(), 'link' => $post->getGuid()));
        }

        return new JsonResponse(array('status' => false, 'msj' => 'Posts not found by given date and category'));
    }

    /**
     * @Route("/siteCheckAjaxOld/{category}/{date}", name="siteCheckAjaxOld")
     * @param string $category Category name provided by front-end
     * @param string $date (Y-m-d) Post/Show date provided by front-end
     * @return bool JsonResponse
     * @example http://site/siteCheckAjaxOld/Featured/2015-02-28%2009:00 (2015-02-28 09:00)
     */
    public function siteCheckOld($category = '', $date = '')
    {
        if (!$date)
            $date = new \DateTime('now');
        else
            $date = new \DateTime($date);

        // Check for category name provided by calendar
        $termManager = $this->get('ekino.wordpress.manager.term');
        $term = $termManager->findOneBy(array('name' => $category));
        if (!$term)
            return new JsonResponse(array('status' => false, 'msj' => 'Term not found'));

        // Is a real category?
        $tax = $this->isCategory($term->getId());
        if (!$tax)
            return new JsonResponse(array('status' => false));

        // Last post from current category
        $post = $this->getLastPost($tax);
        if (!$post)
            return new JsonResponse(array('status' => false));

        return ($post->getDate()->format('Ymdhi') == $date->format('Ymdhi')) ? new JsonResponse(array('status' => true)) : new JsonResponse(array('status' => false, 'link' => $post->getGuid(), 'title' => $post->getTitle(), 'id' => $post->getId(), 'date' => $post->getDate()->format('Ymdhi')));
    }

    /**
     * @param $term
     * @return bool/int ID
     */
    private function isCategory($term)
    {
        $taxManager = $this->get('ekino.wordpress.manager.term_taxonomy');
        $tax = $taxManager->findOneBy(array('term' => $term, 'taxonomy' => 'category'));

        return ($tax) ? $tax->getId() : false;
    }

    /**
     * @param int $catTax
     * @return bool/Post $post
     */
    private function getLastPost($catTax)
    {
        $relationManager = $this->get('ekino.wordpress.manager.term_relationships');
        $relations = $relationManager->findBy(array('taxonomy' => $catTax));

        return (!empty($relations)) ? array_pop($relations)->getPost() : false;
    }

    /**
     * @param int $post
     * @param int $catTax
     * @return bool/Post $post
     */
    private function isPostCategory($post, $catTax)
    {
        $relationManager = $this->get('ekino.wordpress.manager.term_relationships');
        $relation = $relationManager->findOneBy(array('taxonomy' => $catTax, 'post' => $post));

        return (!empty($relation)) ? true : false;
    }

    /**
     * @Route("/sitecheck",
     * name="sitecheck",
     * methods = { "GET", "POST" })
     */
    // public function siteCheckAction(Request $request)
    // {
    //   $termManager = $this->get('ekino.wordpress.manager.term');
    //   $postManager = $this->get('ekino.wordpress.manager.post');

    // $_GET parameters
    // $request->query->get('name');

    // $_POST parameters
    // $request->request->get('name');

    //   $showName = $termManager->findOneBy(
    //     array('name' => $request->request->get('showName'))
    //     );

    //   $post = $postManager->find(23033);
    //   $postCat = $post->getCategory();
    //   return new JsonResponse(array(
    //     'postStatus' => $showName->getId()
    //     ));
    // }
}
