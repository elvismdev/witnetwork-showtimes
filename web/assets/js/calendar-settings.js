// var ajaxCall = function(eventData){
//     $.get( "siteCheckAjax", function( data ) {
//         // eventData['wpPost'] = data;
//         // $.extend( eventData, data );
//      // console.log(eventData.wpPost.status);
//  });
// };


$(function () {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var webData; //array() data

  $('#calendar-holder').fullCalendar({
    // eventDataTransform: function(eventData){
        // ajaxCall(eventData);
        // wpPost = eventData;
        // console.log(eventData);
        // if (eventData.wpPost.id) {
            // eventData.color = 'white';
        // };
        // if(eventData.end.isBefore(moment())){
            // eventData.color = 'black';
            // eventData.url = 'linkkkkk';
            // eventData.status = 'Published';
        // }else{
        //     eventData.color = "green";
        // }
        // $.ajax({
        //     type: 'GET',
        //     url: '/siteCheckAjax'
        // }).done(
        // function(data){
        //     // alert(data.status);
        //     // if (data.status !== true) {
        //         // eventData.color = 'black';
        //     // }
        // });

// return eventData;
// },
allDaySlot: false,
minTime: "09:00:00",
maxTime: "22:00:00",
eventRender: function(event, element) {
  var dataHoje = new Date();

  // console.log(event.start);

  category = event.title.split(' - ')[0];
  dateEvent = event.start.format('YYYY-MM-DD');

  // console.log(category);

  $.ajax({
    type: 'GET',
    url: 'siteCheckAjax/'+category+'/'+dateEvent
}).done(
function(data){
    if (data.status) {
        if (event.start < dataHoje && event.end > dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#0bed00');
            element.find('.fc-event-inner').append('<span class="fc-event-status">ON AIR</span>');
        } else if (event.start < dataHoje && event.end < dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#b2b2b2');
            element.find('.fc-event-inner').append('<span class="fc-event-status">Published</span>');
        } else if (event.start > dataHoje && event.end > dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#00ceed');
            element.find('.fc-event-inner').append('<span class="fc-event-status">Scheduled</span>');
        }
    } else {
        // console.log(data);
        element.attr( 'href', '' );
        element.css({'background-color':'#ed0000', 'cursor':'default'});
        element.find('.fc-event-inner').append('<span class="fc-event-status">Not Scheduled</span>');
    };
});
      // console.log(event.status);
      /*
        Use webData data taken with ajax on eventAfterAllRender callback option
        inside this conditional statements to draw on the event box
        colors and text values depending on the status and date of the post returned.
        */
        // if (event.start < dataHoje && event.end > dataHoje) {
        //     element.css('background-color', '#FFB347');
        //     element.find('.fc-event-inner').append('<span class="fc-event-status">ON AIR</span>');
        // } else if (event.start < dataHoje && event.end < dataHoje) {
        //     element.css('background-color', '#77DD77');
        //     element.find('.fc-event-inner').append('<span class="fc-event-status">'+event.status+'</span>');
        // } else if (event.start > dataHoje && event.end > dataHoje) {
        //     element.css('background-color', '#AEC6CF');
        //     element.find('.fc-event-inner').append('<span class="fc-event-status">Scheduled</span>');
        // }
    },
    eventAfterAllRender: function () {
        webData = '(AJAX CALL TO WEBSITE POSTS I THINK SHOULD GO HERE)';

        // $.ajax({
        //     type: 'GET',
        //     url: '/siteCheckAjax'
        // }).done(
        // function(data){
        //     // console.log(data);
        // });
},
eventColor: '#000',
complete: function() {

},
defaultView: 'agendaDay',
googleCalendarApiKey: 'AIzaSyCtEQZsFtsY41kJ1Av5FftgX9u0hTH83WY',
events: {
  googleCalendarId: 'grantcardone.com_l84tadr5fulc7j0628g3g6oj3k@group.calendar.google.com'
},
header: {
  left: 'prev, next,',
  center: 'title',
  right: 'today'
},
titleFormat: '[<span class="showtime-title">#Showtime</span><span class="showtime-date">]MMMM DD YYYY[</span>]',
lazyFetching: true,
axisFormat: 'h(:mm)a',
timeFormat: {
            agenda: 'h:mmt',    // 5:00 - 6:30
            '': 'h:mmt'         // 7p
        },
        weekNumbers: false,
        lang: 'en',
        eventSources: [
        {
          url: Routing.generate('fullcalendar_loader'),
          type: 'POST',
          data: {
          },
          error: function() {
          }
      }
      ]
  });


});

var refreshRate;

function reloadTime() {
  refreshRate = setTimeout(reloadPage, 60000);
}

function reloadPage() {
  $("#calendar-holder").fullCalendar("refetchEvents");
  reloadTime();
}

$( document ).ready(function() {
  reloadTime();
});